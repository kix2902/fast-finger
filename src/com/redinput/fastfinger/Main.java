package com.redinput.fastfinger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.SignInButton;
import com.google.example.games.basegameutils.BaseGameActivity;

public class Main extends BaseGameActivity implements OnClickListener {

	private static final int REQUEST_LEADERBOARD = 0;
	private static final int REQUEST_ACHIEVEMENTS = 1;

	private Button btnPulsar;
	private TextView txtClicks, txtSegundos;
	private int contador;
	private long remainingSecs;
	private CuentaAtras counter;

	private Button signout;
	private SignInButton signin;
	private ImageButton btnGPG;

	private Boolean finished;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		signout = (Button) findViewById(R.id.sign_out_button);
		signout.setOnClickListener(this);

		btnGPG = (ImageButton) findViewById(R.id.btnGPG);
		btnGPG.setOnClickListener(this);

		signin = (SignInButton) findViewById(R.id.sign_in_button);
		signin.setOnClickListener(this);

		btnPulsar = (Button) findViewById(R.id.btnPulsar);
		btnPulsar.setOnClickListener(this);

		txtClicks = (TextView) findViewById(R.id.txtClicks);
		txtSegundos = (TextView) findViewById(R.id.txtSegundos);

		ReseteaContador();

		if (savedInstanceState != null) {
			contador = savedInstanceState.getInt("contador");
			remainingSecs = savedInstanceState.getLong("remaining", -1);

			txtClicks.setText(contador + " " + getResources().getString(R.string.clicks));
			if (remainingSecs >= 0) {
				txtSegundos.setText(remainingSecs + "");
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		counter.cancel();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if ((!finished) && (contador > 0)) {
			counter = new CuentaAtras(remainingSecs, 1000);
			counter.start();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putInt("contador", contador);
		if (remainingSecs != 10000) {
			outState.putLong("remaining", remainingSecs);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnPulsar:

			if (contador == 0) {
				counter.start();
			}

			contador++;
			txtClicks.setText(contador + " " + getResources().getString(R.string.clicks));

			break;

		case R.id.btnGPG:

			AlertDialog.Builder buildGPG = new AlertDialog.Builder(this);
			buildGPG.setCancelable(true);
			buildGPG.setTitle("Google Play Games");

			buildGPG.setPositiveButton(R.string.leaderboard, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

					startActivityForResult(getGamesClient().getLeaderboardIntent(getString(R.string.leaderboard_id)), REQUEST_LEADERBOARD);
				}
			});
			buildGPG.setNeutralButton(R.string.achievements, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

					startActivityForResult(getGamesClient().getAchievementsIntent(), REQUEST_ACHIEVEMENTS);
				}
			});

			buildGPG.create().show();

			break;

		case R.id.sign_in_button:
			beginUserInitiatedSignIn();

			break;

		case R.id.sign_out_button:
			signOut();

			// show sign-in button, hide the sign-out button
			signin.setVisibility(View.VISIBLE);
			signout.setVisibility(View.GONE);
			btnGPG.setVisibility(View.GONE);
		}
	}

	private class CuentaAtras extends CountDownTimer {

		public CuentaAtras(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			remainingSecs = millisInFuture;
		}

		@Override
		public void onFinish() {

			finished = true;

			if (isSignedIn()) {
				getGamesClient().submitScore(getString(R.string.leaderboard_id), contador);

				getGamesClient().incrementAchievement(getString(R.string.achievement_id_5), 1);
				getGamesClient().incrementAchievement(getString(R.string.achievement_id_25), 1);

				if (contador == 1) {
					getGamesClient().unlockAchievement(getString(R.string.achievement_id_dont));

				} else if (contador >= 75) {
					getGamesClient().unlockAchievement(getString(R.string.achievement_id_75));
				}
			}

			btnPulsar.setEnabled(false);
			txtSegundos.setText("0");

			CustomDialogBuilder builder = new CustomDialogBuilder(Main.this);
			builder.setCancelable(false);
			builder.setTitle(R.string.title_dialog);
			builder.setMessage(getResources().getString(R.string.description_dialog1) + " " + contador + " " + getResources().getString(R.string.description_dialog2));
			builder.setNegativeButton(R.string.cerrar, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

					ReseteaContador();
				}
			});
			builder.setPositiveButton(R.string.compartir, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (isSignedIn()) {
						getGamesClient().unlockAchievement(getString(R.string.achievement_id_share));
					}

					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.texto_compartir1) + " " + contador + " " + getResources().getString(R.string.texto_compartir2));

					startActivity(Intent.createChooser(i, getResources().getString(R.string.selector)));
				}
			});

			builder.show();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			txtSegundos.setText(String.valueOf(Math.round(millisUntilFinished / 1000)));
			remainingSecs = millisUntilFinished;
		}

	}

	private void ReseteaContador() {
		finished = false;
		contador = 0;
		remainingSecs = 0;
		counter = new CuentaAtras(10000, 1000);
		btnPulsar.setEnabled(true);
		txtClicks.setText(R.string.texto_click);
		txtSegundos.setText(R.string.texto_segundos);
	}

	@Override
	public void onSignInSucceeded() {
		// show sign-out button, hide the sign-in button
		signin.setVisibility(View.GONE);
		signout.setVisibility(View.VISIBLE);
		btnGPG.setVisibility(View.VISIBLE);
	}

	@Override
	public void onSignInFailed() {
		// Sign in has failed. So show the user the sign-in button.
		signin.setVisibility(View.VISIBLE);
		signout.setVisibility(View.GONE);
		btnGPG.setVisibility(View.GONE);
	}

}
